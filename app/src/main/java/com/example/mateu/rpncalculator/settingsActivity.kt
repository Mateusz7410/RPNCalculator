package com.example.mateu.rpncalculator

import android.graphics.Color
import android.graphics.PorterDuff
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.NumberPicker
import android.widget.Spinner
import android.support.v7.widget.Toolbar
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

class settingsActivity : AppCompatActivity() {
    internal lateinit var operationButtonsBackgroundSpinner: Spinner
    internal lateinit var operationButtonsTextSpinner: Spinner
    internal lateinit var restButtonsBackgroundSpinner: Spinner
    internal lateinit var restButtonsTextSpinner: Spinner
    internal lateinit var windowBackgroundSpinner: Spinner
    internal lateinit var windowTextSpinner: Spinner
    internal lateinit var applicationBackgroundSpinner: Spinner
    internal lateinit var done: Button
    internal lateinit var mainWindow: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        mainWindow = findViewById<View>(R.id.mainWindow) as ConstraintLayout
        mainWindow.setBackgroundColor(resources.getColor(R.color.lightGray))

        val myToolbar = findViewById<View>(R.id.my_toolbar) as Toolbar
        setSupportActionBar(myToolbar)
        title = "Options"
        myToolbar.setBackgroundColor(resources.getColor(R.color.pink))

        done = findViewById<View>(R.id.done) as Button

        done.setOnClickListener { finish() }

        done.background.setColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY)
        done.setTextColor(Color.WHITE)

        val arraySpinner = ArrayList<String>()
        arraySpinner.add("Black")
        arraySpinner.add("White")
        arraySpinner.add("Blue")
        arraySpinner.add("Green")
        arraySpinner.add("Magenta")
        arraySpinner.add("Yellow")
        arraySpinner.add("Red")
        arraySpinner.add("Gray")

        operationButtonsBackgroundSpinner = findViewById<View>(R.id.operationButtonsBackgroundSpinner) as Spinner
        val adapter = object : ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner) {

            override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View? {
                var v: TextView? = null
                v = super.getDropDownView(position, null, parent) as TextView
                if (position == 0) v.run {
                    setBackgroundColor(Color.BLACK)
                    setTextColor(Color.BLACK)
                } else if (position == 1) v.run {
                    setBackgroundColor(Color.WHITE)
                    setTextColor(Color.WHITE)
                } else if (position == 2) v.run {
                    setBackgroundColor(Color.BLUE)
                    setTextColor(Color.BLUE)
                } else if (position == 3) v.run {
                    setBackgroundColor(Color.GREEN)
                    setTextColor(Color.GREEN)
                } else if (position == 4) v.run {
                    setBackgroundColor(Color.MAGENTA)
                    setTextColor(Color.MAGENTA)
                } else if (position == 5) v.run {
                    setBackgroundColor(Color.YELLOW)
                    setTextColor(Color.YELLOW)
                } else if (position == 6) v.run {
                    setBackgroundColor(Color.RED)
                    setTextColor(Color.RED)
                } else if (position == 7) v.run {
                    setBackgroundColor(Color.GRAY)
                    setTextColor(Color.GRAY)
                }
                return v
            }
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        operationButtonsBackgroundSpinner.adapter = adapter
        operationButtonsBackgroundSpinner.setSelection(arraySpinner.indexOf(Container.operationButtonsBackgroundS))

        operationButtonsTextSpinner = findViewById<View>(R.id.operationButtonsTextSpinner) as Spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        operationButtonsTextSpinner.adapter = adapter
        operationButtonsTextSpinner.setSelection(arraySpinner.indexOf(Container.operationButtonsTextS))

        restButtonsBackgroundSpinner = findViewById<View>(R.id.restButtonsBackgroundSpinner) as Spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        restButtonsBackgroundSpinner.adapter = adapter
        restButtonsBackgroundSpinner.setSelection(arraySpinner.indexOf(Container.restButtonsBackgroundS))

        restButtonsTextSpinner = findViewById<View>(R.id.restButtonsTextSpinner) as Spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        restButtonsTextSpinner.adapter = adapter
        restButtonsTextSpinner.setSelection(arraySpinner.indexOf(Container.restButtonsTextS))

        windowBackgroundSpinner = findViewById<View>(R.id.windowBackgroundSpinner) as Spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        windowBackgroundSpinner.adapter = adapter
        windowBackgroundSpinner.setSelection(arraySpinner.indexOf(Container.windowBackgroundS))

        windowTextSpinner = findViewById<View>(R.id.windowTextSpinner) as Spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        windowTextSpinner.adapter = adapter
        windowTextSpinner.setSelection(arraySpinner.indexOf(Container.windowTextS))

        applicationBackgroundSpinner = findViewById<View>(R.id.applicationBackgroundSpinner) as Spinner
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        applicationBackgroundSpinner.adapter = adapter
        applicationBackgroundSpinner.setSelection(arraySpinner.indexOf(Container.applicationBackgroundS))


        operationButtonsBackgroundSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "Black") {
                    Container.operationButtonsBackground = Color.BLACK
                    Container.operationButtonsBackgroundS = "Black"
                }
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "White") {
                    Container.operationButtonsBackground = Color.WHITE
                    Container.operationButtonsBackgroundS = "White"
                }
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "Blue") {
                    Container.operationButtonsBackground = Color.BLUE
                    Container.operationButtonsBackgroundS = "Blue"
                }
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "Green") {
                    Container.operationButtonsBackground = Color.GREEN
                    Container.operationButtonsBackgroundS = "Green"
                }
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "Magenta") {
                    Container.operationButtonsBackground = Color.MAGENTA
                    Container.operationButtonsBackgroundS = "Magenta"
                }
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "Yellow") {
                    Container.operationButtonsBackground = Color.YELLOW
                    Container.operationButtonsBackgroundS = "Yellow"
                }
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "Red") {
                    Container.operationButtonsBackground = Color.RED
                    Container.operationButtonsBackgroundS = "Red"
                }
                if (operationButtonsBackgroundSpinner.selectedItem.toString() == "Gray") {
                    Container.operationButtonsBackground = Color.GRAY
                    Container.operationButtonsBackgroundS = "Gray"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        operationButtonsTextSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (operationButtonsTextSpinner.selectedItem.toString() == "Black") {
                    Container.operationButtonsText = Color.BLACK
                    Container.operationButtonsTextS = "Black"
                }
                if (operationButtonsTextSpinner.selectedItem.toString() == "White") {
                    Container.operationButtonsText = Color.WHITE
                    Container.operationButtonsTextS = "White"
                }
                if (operationButtonsTextSpinner.selectedItem.toString() == "Blue") {
                    Container.operationButtonsText = Color.BLUE
                    Container.operationButtonsTextS = "Blue"
                }
                if (operationButtonsTextSpinner.selectedItem.toString() == "Green") {
                    Container.operationButtonsText = Color.GREEN
                    Container.operationButtonsTextS = "Green"
                }
                if (operationButtonsTextSpinner.selectedItem.toString() == "Magenta") {
                    Container.operationButtonsText = Color.MAGENTA
                    Container.operationButtonsTextS = "Magenta"
                }
                if (operationButtonsTextSpinner.selectedItem.toString() == "Yellow") {
                    Container.operationButtonsText = Color.YELLOW
                    Container.operationButtonsTextS = "Yellow"
                }
                if (operationButtonsTextSpinner.selectedItem.toString() == "Red") {
                    Container.operationButtonsText = Color.RED
                    Container.operationButtonsTextS = "Red"
                }
                if (operationButtonsTextSpinner.selectedItem.toString() == "Gray") {
                    Container.operationButtonsText = Color.GRAY
                    Container.operationButtonsTextS = "Gray"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        restButtonsTextSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (restButtonsTextSpinner.selectedItem.toString() == "Black") {
                    Container.restButtonsText = Color.BLACK
                    Container.restButtonsTextS = "Black"
                }
                if (restButtonsTextSpinner.selectedItem.toString() == "White") {
                    Container.restButtonsText = Color.WHITE
                    Container.restButtonsTextS = "White"
                }
                if (restButtonsTextSpinner.selectedItem.toString() == "Blue") {
                    Container.restButtonsText = Color.BLUE
                    Container.restButtonsTextS = "Blue"
                }
                if (restButtonsTextSpinner.selectedItem.toString() == "Green") {
                    Container.restButtonsText = Color.GREEN
                    Container.restButtonsTextS = "Green"
                }
                if (restButtonsTextSpinner.selectedItem.toString() == "Magenta") {
                    Container.restButtonsText = Color.MAGENTA
                    Container.restButtonsTextS = "Magenta"
                }
                if (restButtonsTextSpinner.selectedItem.toString() == "Yellow") {
                    Container.restButtonsText = Color.YELLOW
                    Container.restButtonsTextS = "Yellow"
                }
                if (restButtonsTextSpinner.selectedItem.toString() == "Red") {
                    Container.restButtonsText = Color.RED
                    Container.restButtonsTextS = "Red"
                }
                if (restButtonsTextSpinner.selectedItem.toString() == "Gray") {
                    Container.restButtonsText = Color.GRAY
                    Container.restButtonsTextS = "Gray"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        restButtonsBackgroundSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "Black") {
                    Container.restButtonsBackground = Color.BLACK
                    Container.restButtonsBackgroundS = "Black"
                }
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "White") {
                    Container.restButtonsBackground = Color.WHITE
                    Container.restButtonsBackgroundS = "White"
                }
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "Blue") {
                    Container.restButtonsBackground = Color.BLUE
                    Container.restButtonsBackgroundS = "Blue"
                }
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "Green") {
                    Container.restButtonsBackground = Color.GREEN
                    Container.restButtonsBackgroundS = "Green"
                }
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "Magenta") {
                    Container.restButtonsBackground = Color.MAGENTA
                    Container.restButtonsBackgroundS = "Magenta"
                }
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "Yellow") {
                    Container.restButtonsBackground = Color.YELLOW
                    Container.restButtonsBackgroundS = "Yellow"
                }
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "Red") {
                    Container.restButtonsBackground = Color.RED
                    Container.restButtonsBackgroundS = "Red"
                }
                if (restButtonsBackgroundSpinner.selectedItem.toString() == "Gray") {
                    Container.restButtonsBackground = Color.GRAY
                    Container.restButtonsBackgroundS = "Gray"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        windowBackgroundSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (windowBackgroundSpinner.selectedItem.toString() == "Black") {
                    Container.windowBackground = Color.BLACK
                    Container.windowBackgroundS = "Black"
                }
                if (windowBackgroundSpinner.selectedItem.toString() == "White") {
                    Container.windowBackground = Color.WHITE
                    Container.windowBackgroundS = "White"
                }
                if (windowBackgroundSpinner.selectedItem.toString() == "Blue") {
                    Container.windowBackground = Color.BLUE
                    Container.windowBackgroundS = "Blue"
                }
                if (windowBackgroundSpinner.selectedItem.toString() == "Green") {
                    Container.windowBackground = Color.GREEN
                    Container.windowBackgroundS = "Green"
                }
                if (windowBackgroundSpinner.selectedItem.toString() == "Magenta") {
                    Container.windowBackground = Color.MAGENTA
                    Container.windowBackgroundS = "Magenta"
                }
                if (windowBackgroundSpinner.selectedItem.toString() == "Yellow") {
                    Container.windowBackground = Color.YELLOW
                    Container.windowBackgroundS = "Yellow"
                }
                if (windowBackgroundSpinner.selectedItem.toString() == "Red") {
                    Container.windowBackground = Color.RED
                    Container.windowBackgroundS = "Red"
                }
                if (windowBackgroundSpinner.selectedItem.toString() == "Gray") {
                    Container.windowBackground = Color.GRAY
                    Container.windowBackgroundS = "Gray"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        windowTextSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (windowTextSpinner.selectedItem.toString() == "Black") {
                    Container.windowText = Color.BLACK
                    Container.windowTextS = "Black"
                }
                if (windowTextSpinner.selectedItem.toString() == "White") {
                    Container.windowText = Color.WHITE
                    Container.windowTextS = "White"
                }
                if (windowTextSpinner.selectedItem.toString() == "Blue") {
                    Container.windowText = Color.BLUE
                    Container.windowTextS = "Blue"
                }
                if (windowTextSpinner.selectedItem.toString() == "Green") {
                    Container.windowText = Color.GREEN
                    Container.windowTextS = "Green"
                }
                if (windowTextSpinner.selectedItem.toString() == "Magenta") {
                    Container.windowText = Color.MAGENTA
                    Container.windowTextS = "Magenta"
                }
                if (windowTextSpinner.selectedItem.toString() == "Yellow") {
                    Container.windowText = Color.YELLOW
                    Container.windowTextS = "Yellow"
                }
                if (windowTextSpinner.selectedItem.toString() == "Red") {
                    Container.windowText = Color.RED
                    Container.windowTextS = "Red"
                }
                if (windowTextSpinner.selectedItem.toString() == "Gray") {
                    Container.windowText = Color.GRAY
                    Container.windowTextS = "Gray"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        applicationBackgroundSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (applicationBackgroundSpinner.selectedItem.toString() == "Black") {
                    Container.applicationBackground = Color.BLACK
                    Container.applicationBackgroundS = "Black"
                }
                if (applicationBackgroundSpinner.selectedItem.toString() == "White") {
                    Container.applicationBackground = Color.WHITE
                    Container.applicationBackgroundS = "White"
                }
                if (applicationBackgroundSpinner.selectedItem.toString() == "Blue") {
                    Container.applicationBackground = Color.BLUE
                    Container.applicationBackgroundS = "Blue"
                }
                if (applicationBackgroundSpinner.selectedItem.toString() == "Green") {
                    Container.applicationBackground = Color.GREEN
                    Container.applicationBackgroundS = "Green"
                }
                if (applicationBackgroundSpinner.selectedItem.toString() == "Magenta") {
                    Container.applicationBackground = Color.MAGENTA
                    Container.applicationBackgroundS = "Magenta"
                }
                if (applicationBackgroundSpinner.selectedItem.toString() == "Yellow") {
                    Container.applicationBackground = Color.YELLOW
                    Container.applicationBackgroundS = "Yellow"
                }
                if (applicationBackgroundSpinner.selectedItem.toString() == "Red") {
                    Container.applicationBackground = Color.RED
                    Container.applicationBackgroundS = "Red"
                }
                if (applicationBackgroundSpinner.selectedItem.toString() == "Gray") {
                    Container.applicationBackground = Color.GRAY
                    Container.applicationBackgroundS = "Gray"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


        val np = findViewById<View>(R.id.numberPicker2) as NumberPicker

        np.minValue = 1
        np.maxValue = 8
        np.wrapSelectorWheel = true
        np.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        np.value = Container.precision


        np.setOnValueChangedListener { picker, oldVal, newVal -> Container.precision = np.value }
    }


}
