package com.example.mateu.rpncalculator

import android.app.Activity
import android.content.Intent
import android.graphics.PorterDuff
import android.support.constraint.ConstraintLayout
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import java.text.DecimalFormat
import java.util.ArrayList
import java.util.Stack

class MainActivity : Activity() {
    internal var number0: Button? = null
    internal var number1: Button? = null
    internal var number2: Button? = null
    internal var number3: Button? = null
    internal var number4: Button? = null
    internal var number5: Button? = null
    internal var number6: Button? = null
    internal var number7: Button? = null
    internal var number8: Button? = null
    internal var number9: Button? = null
    internal var plusMinus: Button? = null
    internal var dot: Button? = null
    internal var plus: Button? = null
    internal var minus: Button? = null
    internal var multiplication: Button? = null
    internal var division: Button? = null
    internal var powyx: Button? = null
    internal var sqrt: Button? = null
    internal var enter: Button? = null
    internal var drop: Button? = null
    internal var undo: Button? = null
    internal var cac: Button? = null
    internal var delete: Button? = null
    internal var swap: Button? = null
    internal var options: Button? = null

    internal var currentValue: TextView? = null
    internal var currentValueLabel: TextView? = null
    internal var stack1: TextView? = null
    internal var stack2: TextView? = null
    internal var stack3: TextView? = null
    internal var stack4: TextView? = null
    internal var stackLabel1: TextView? = null
    internal var stackLabel2: TextView? = null
    internal var stackLabel3: TextView? = null
    internal var stackLabel4: TextView? = null
    internal lateinit var printWindow: LinearLayout
    internal lateinit var buttonWindow: LinearLayout
    internal lateinit var mainWindow: ConstraintLayout

    internal var undoStack = Stack<ArrayList<String>>()
    internal var values = Stack<Double>()
    internal var cancelStack = Stack<Stack<Double>>()

    internal lateinit var precision: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)

        number0 = findViewById<View>(R.id.number0) as Button
        number1 = findViewById<View>(R.id.number1) as Button
        number2 = findViewById<View>(R.id.number2) as Button
        number3 = findViewById<View>(R.id.number3) as Button
        number4 = findViewById<View>(R.id.number4) as Button
        number5 = findViewById<View>(R.id.number5) as Button
        number6 = findViewById<View>(R.id.number6) as Button
        number7 = findViewById<View>(R.id.number7) as Button
        number8 = findViewById<View>(R.id.number8) as Button
        number9 = findViewById<View>(R.id.number9) as Button
        plusMinus = findViewById<View>(R.id.plusMinus) as Button
        dot = findViewById<View>(R.id.dot) as Button
        plus = findViewById<View>(R.id.plus) as Button
        minus = findViewById<View>(R.id.minus) as Button
        multiplication = findViewById<View>(R.id.multiplication) as Button
        division = findViewById<View>(R.id.division) as Button
        powyx = findViewById<View>(R.id.powxy) as Button
        sqrt = findViewById<View>(R.id.sqrt) as Button
        enter = findViewById<View>(R.id.enter) as Button
        drop = findViewById<View>(R.id.drop) as Button
        undo = findViewById<View>(R.id.undo) as Button
        cac = findViewById<View>(R.id.cac) as Button
        delete = findViewById<View>(R.id.delete) as Button
        swap = findViewById<View>(R.id.swap) as Button
        options = findViewById<View>(R.id.options) as Button

        currentValue = findViewById<View>(R.id.currentValue) as TextView
        currentValueLabel = findViewById<View>(R.id.currentValueLabel) as TextView
        stack1 = findViewById<View>(R.id.stack1) as TextView
        stack2 = findViewById<View>(R.id.stack2) as TextView
        stack3 = findViewById<View>(R.id.stack3) as TextView
        stack4 = findViewById<View>(R.id.stack4) as TextView
        stackLabel1 = findViewById<View>(R.id.stackLabel1) as TextView
        stackLabel2 = findViewById<View>(R.id.stackLabel2) as TextView
        stackLabel3 = findViewById<View>(R.id.stackLabel3) as TextView
        stackLabel4 = findViewById<View>(R.id.stackLabel4) as TextView
        printWindow = findViewById<View>(R.id.printWindow) as LinearLayout
        buttonWindow = findViewById<View>(R.id.buttonWindow) as LinearLayout
        mainWindow = findViewById<View>(R.id.mainWindow) as ConstraintLayout
        setChanges()
        val textWatcher = object : TextWatcher {

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {

                if (currentValue!!.text.toString() == "") {
                    currentValue!!.visibility = View.GONE
                    currentValueLabel!!.visibility = View.GONE
                } else {
                    currentValue!!.visibility = View.VISIBLE
                    currentValueLabel!!.visibility = View.VISIBLE
                }

            }
        }

        currentValue!!.addTextChangedListener(textWatcher)

        currentValue!!.text = ""
        stack1!!.text = "-"
        stack2!!.text = "-"
        stack3!!.text = "-"
        stack4!!.text = "-"


        number0!!.setOnClickListener {
            if (currentValue!!.text.toString() != "0")
                currentValue!!.append("0")
        }

        number1!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "1"
            else
                currentValue!!.append("1")
        }

        number2!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "2"
            else
                currentValue!!.append("2")
        }

        number3!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "3"
            else
                currentValue!!.append("3")
        }

        number4!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "4"
            else
                currentValue!!.append("4")
        }

        number5!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "5"
            else
                currentValue!!.append("5")
        }

        number6!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "6"
            else
                currentValue!!.append("6")
        }

        number7!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "7"
            else
                currentValue!!.append("7")
        }

        number8!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "8"
            else
                currentValue!!.append("8")
        }

        number9!!.setOnClickListener {
            if (currentValue!!.text.toString() == "0" || currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity")
                currentValue!!.text = "9"
            else
                currentValue!!.append("9")
        }

        plusMinus!!.setOnClickListener {
            if (!values.empty()) {
                if (undoStack.empty()) {
                    val tmp = ArrayList<String>()
                    tmp.add("plusMinus")
                    undoStack.push(tmp)
                } else {
                    if (undoStack.peek()[0] != "plusMinus") {
                        val tmp = ArrayList<String>()
                        tmp.add("plusMinus")
                        undoStack.push(tmp)
                    } else {
                        undoStack.pop()
                    }
                }
                values.push(-values.pop())
                printValues()
            }
        }

        dot!!.setOnClickListener {
            if (!currentValue!!.text.toString().contains("."))
                currentValue!!.append(".")
        }

        plus!!.setOnClickListener {
            if (values.size >= 2) {
                val tmp = ArrayList<String>()
                tmp.add("plus")
                val tmp1 = values.pop()
                tmp.add(java.lang.Double.toString(tmp1))
                tmp.add(values.peek().toString())
                values.push(tmp1)
                undoStack.push(tmp)

                val d = values.pop() + values.pop()
                values.push(d)
                currentValue!!.text = ""
                printValues()
            }
        }

        minus!!.setOnClickListener {
            if (values.size >= 2) {
                val tmp = ArrayList<String>()
                tmp.add("minus")
                var tmp1 = values.pop()
                tmp.add(java.lang.Double.toString(tmp1))
                tmp.add(values.peek().toString())
                values.push(tmp1)
                undoStack.push(tmp)

                tmp1 = values.pop()
                val d = values.pop() - tmp1
                values.push(d)
                currentValue!!.text = ""
                printValues()
            }
        }

        multiplication!!.setOnClickListener {
            if (values.size >= 2) {
                val tmp = ArrayList<String>()
                tmp.add("multiplication")
                val tmp1 = values.pop()
                tmp.add(java.lang.Double.toString(tmp1))
                tmp.add(values.peek().toString())
                values.push(tmp1)
                undoStack.push(tmp)


                val d = values.pop() * values.pop()
                values.push(d)
                currentValue!!.text = ""
                printValues()
            }
        }

        division!!.setOnClickListener {
            if (values.size >= 2) {
                var tmp1 = values.pop()
                val div = values.peek() / tmp1
                if (div.isNaN() || div.isInfinite() || div == java.lang.Double.POSITIVE_INFINITY || div == java.lang.Double.NEGATIVE_INFINITY) {
                    Toast.makeText(this@MainActivity, "You can not divide by zero!", Toast.LENGTH_SHORT).show()
                } else {
                    val tmp = ArrayList<String>()
                    tmp.add("division")
                    tmp.add(java.lang.Double.toString(tmp1))
                    tmp.add(values.peek().toString())
                    values.push(tmp1)
                    undoStack.push(tmp)

                    tmp1 = values.pop()
                    val d = values.pop() / tmp1
                    values.push(d)
                    currentValue!!.text = ""
                    printValues()
                }
            }
        }

        powyx!!.setOnClickListener {
            if (values.size >= 2) {
                val tmp = ArrayList<String>()
                tmp.add("powyx")
                var tmp1 = values.pop()
                tmp.add(java.lang.Double.toString(tmp1))
                tmp.add(values.peek().toString())
                values.push(tmp1)
                undoStack.push(tmp)

                tmp1 = values.pop()
                val d = Math.pow(values.pop(), tmp1)
                values.push(d)
                currentValue!!.text = ""
                printValues()
            }
        }

        sqrt!!.setOnClickListener {
            if (!values.empty()) {
                val tmp = ArrayList<String>()
                tmp.add("sqrt")
                tmp.add(java.lang.Double.toString(values.peek()))
                undoStack.push(tmp)

                val d = Math.sqrt(values.pop())
                values.push(d)
                currentValue!!.text = ""
                printValues()
            }
        }

        enter!!.setOnClickListener {
            if (currentValue!!.text.toString() == "" && !values.empty()) {
                val tmp = ArrayList<String>()
                tmp.add("enter")
                undoStack.push(tmp)

                values.push(values.peek())
                currentValue!!.text = ""
                printValues()
            } else if (currentValue!!.text.toString() != "") {
                val tmp = ArrayList<String>()
                tmp.add("enter")
                undoStack.push(tmp)

                values.push(java.lang.Double.parseDouble(currentValue!!.text.toString()))
                currentValue!!.text = ""
                printValues()
            } else {
                Toast.makeText(this@MainActivity, "There is no number to enter!", Toast.LENGTH_SHORT).show()
            }
        }

        drop!!.setOnClickListener {
            if (!values.empty()) {
                val tmp = ArrayList<String>()
                tmp.add("drop")
                tmp.add(values.peek().toString())
                undoStack.push(tmp)

                values.pop()
                printValues()
            }
        }

        swap!!.setOnClickListener {
            if (values.size >= 2) {
                if (!undoStack.empty()) {
                    if (undoStack.peek()[0] != "swap") {
                        val tmp2 = ArrayList<String>()
                        tmp2.add("swap")
                        undoStack.push(tmp2)
                    } else {
                        undoStack.pop()
                    }
                }

                val a: Double?
                val b: Double?
                a = values.pop()
                b = values.pop()
                values.push(a)
                values.push(b)
                printValues()
            }
        }

        undo!!.setOnClickListener {
            if (!undoStack.empty()) {
                val args = undoStack.pop()
                val undoMove = args[0]
                when (undoMove) {
                    "enter" -> {
                        val d = values.pop()
                        printValues()
                    }
                    "drop" -> {
                        values.push(java.lang.Double.parseDouble(args[1]))
                        printValues()
                        currentValue!!.text = ""
                    }
                    "swap" -> {
                        val a: Double?
                        val b: Double?
                        a = values.pop()
                        b = values.pop()
                        values.push(a)
                        values.push(b)
                        printValues()
                    }
                    "ca" -> {
                        values = cancelStack.pop()
                        printValues()
                        if (!args[1].isEmpty()) {
                            val d = java.lang.Double.parseDouble(args[1])
                            val s = if (d.toLong().equals(d)) "" + d.toLong() else "" + d
                            currentValue!!.text = s
                        } else
                            currentValue!!.text = ""
                    }
                    "cc" -> {
                        if (!args[1].isEmpty()) {
                            val d = java.lang.Double.parseDouble(args[1])
                            val s = if (d.toLong().equals(d)) "" + d.toLong() else "" + d
                            currentValue!!.text = s
                        } else
                            currentValue!!.text = ""
                    }
                    "plusMinus" -> {
                        values.push(-values.pop())
                        printValues()
                    }
                    "plus" -> {
                        values.pop()
                        values.push(java.lang.Double.parseDouble(args[2]))
                        values.push(java.lang.Double.parseDouble(args[1]))
                        printValues()
                    }
                    "minus" -> {
                        values.pop()
                        values.push(java.lang.Double.parseDouble(args[2]))
                        values.push(java.lang.Double.parseDouble(args[1]))
                        printValues()
                    }
                    "multiplication" -> {
                        values.pop()
                        values.push(java.lang.Double.parseDouble(args[2]))
                        values.push(java.lang.Double.parseDouble(args[1]))
                        printValues()
                    }
                    "division" -> {
                        values.pop()
                        values.push(java.lang.Double.parseDouble(args[2]))
                        values.push(java.lang.Double.parseDouble(args[1]))
                        printValues()
                    }
                    "powyx" -> {
                        values.pop()
                        values.push(java.lang.Double.parseDouble(args[2]))
                        values.push(java.lang.Double.parseDouble(args[1]))
                        printValues()
                    }
                    "sqrt" -> {
                        values.pop()
                        values.push(java.lang.Double.parseDouble(args[1]))
                        printValues()
                    }
                }
            }
        }

        delete!!.setOnClickListener {
            if (currentValue!!.text.toString().length == 1 || currentValue!!.text.toString().length == 2 && currentValue!!.text.toString()[0] == '-')
                currentValue!!.text = ""
            else if (currentValue!!.text.toString() == "NaN" || currentValue!!.text.toString() == "Infinity" || currentValue!!.text.toString() == "") {
                currentValue!!.text = ""
            } else
                currentValue!!.text = currentValue!!.text.toString().substring(0, currentValue!!.text.toString().length - 1)
        }

        cac!!.setOnLongClickListener {
            val tmp = ArrayList<String>()
            tmp.add("ca")
            tmp.add(currentValue!!.text.toString())
            undoStack.push(tmp)
            cancelStack.push(values)

            values = Stack()
            printValues()
            currentValue!!.text = ""
            true
        }

        cac!!.setOnClickListener {
            if (currentValue!!.text.toString() != "0") {
                val tmp = ArrayList<String>()
                tmp.add("cc")
                tmp.add(currentValue!!.text.toString())
                undoStack.push(tmp)
                currentValue!!.text = ""
            }
        }

        options!!.setOnClickListener {
            val myIntent = Intent(this@MainActivity, settingsActivity::class.java)
            this@MainActivity.startActivity(myIntent)
        }
    }

    internal fun printValues() {
        var d: Double?
        val s: String
        val df = DecimalFormat(precision)
        if (values.size == 0) {
            stack1!!.text = "-"
            stack2!!.text = "-"
            stack3!!.text = "-"
            stack4!!.text = "-"
        } else if (values.size == 1) {
            d = values.elementAt(values.size - 1)
            stack1!!.text = df.format(d)
            stack2!!.text = "-"
            stack3!!.text = "-"
            stack4!!.text = "-"
        } else if (values.size == 2) {
            d = values.elementAt(values.size - 1)
            stack1!!.text = df.format(d)
            d = values.elementAt(values.size - 2)
            stack2!!.text = df.format(d)
            stack3!!.text = "-"
            stack4!!.text = "-"
        } else if (values.size == 3) {
            d = values.elementAt(values.size - 1)
            stack1!!.text = df.format(d)
            d = values.elementAt(values.size - 2)
            stack2!!.text = df.format(d)
            d = values.elementAt(values.size - 3)
            stack3!!.text = df.format(d)
            stack4!!.text = "-"
        } else if (values.size >= 4) {
            d = values.elementAt(values.size - 1)
            stack1!!.text = df.format(d)
            d = values.elementAt(values.size - 2)
            stack2!!.text = df.format(d)
            d = values.elementAt(values.size - 3)
            stack3!!.text = df.format(d)
            d = values.elementAt(values.size - 4)
            stack4!!.text = df.format(d)
        }

    }

    override fun onResume() {
        super.onResume()
        setChanges()
        printValues()
    }

    internal fun setChanges() {
        plus!!.background.setColorFilter(Container.operationButtonsBackground, PorterDuff.Mode.MULTIPLY)
        minus!!.background.setColorFilter(Container.operationButtonsBackground, PorterDuff.Mode.MULTIPLY)
        multiplication!!.background.setColorFilter(Container.operationButtonsBackground, PorterDuff.Mode.MULTIPLY)
        division!!.background.setColorFilter(Container.operationButtonsBackground, PorterDuff.Mode.MULTIPLY)
        delete!!.background.setColorFilter(Container.operationButtonsBackground, PorterDuff.Mode.MULTIPLY)

        plus!!.setTextColor(Container.operationButtonsText)
        minus!!.setTextColor(Container.operationButtonsText)
        multiplication!!.setTextColor(Container.operationButtonsText)
        division!!.setTextColor(Container.operationButtonsText)
        delete!!.setTextColor(Container.operationButtonsText)

        cac!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number0!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number1!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number2!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number3!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number4!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number5!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number6!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number7!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number8!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        number9!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        sqrt!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        powyx!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        dot!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        plusMinus!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        enter!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        drop!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        swap!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        undo!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)
        options!!.background.setColorFilter(Container.restButtonsBackground, PorterDuff.Mode.MULTIPLY)

        cac!!.setTextColor(Container.restButtonsText)
        number0!!.setTextColor(Container.restButtonsText)
        number1!!.setTextColor(Container.restButtonsText)
        number2!!.setTextColor(Container.restButtonsText)
        number3!!.setTextColor(Container.restButtonsText)
        number4!!.setTextColor(Container.restButtonsText)
        number5!!.setTextColor(Container.restButtonsText)
        number6!!.setTextColor(Container.restButtonsText)
        number7!!.setTextColor(Container.restButtonsText)
        number8!!.setTextColor(Container.restButtonsText)
        number9!!.setTextColor(Container.restButtonsText)
        sqrt!!.setTextColor(Container.restButtonsText)
        powyx!!.setTextColor(Container.restButtonsText)
        dot!!.setTextColor(Container.restButtonsText)
        plusMinus!!.setTextColor(Container.restButtonsText)
        enter!!.setTextColor(Container.restButtonsText)
        drop!!.setTextColor(Container.restButtonsText)
        swap!!.setTextColor(Container.restButtonsText)
        undo!!.setTextColor(Container.restButtonsText)
        options!!.setTextColor(Container.restButtonsText)

        printWindow.setBackgroundColor(Container.windowBackground)

        currentValue!!.setTextColor(Container.windowText)
        currentValueLabel!!.setTextColor(Container.windowText)
        stack1!!.setTextColor(Container.windowText)
        stack2!!.setTextColor(Container.windowText)
        stack3!!.setTextColor(Container.windowText)
        stack4!!.setTextColor(Container.windowText)
        stackLabel1!!.setTextColor(Container.windowText)
        stackLabel2!!.setTextColor(Container.windowText)
        stackLabel3!!.setTextColor(Container.windowText)
        stackLabel4!!.setTextColor(Container.windowText)

        buttonWindow.setBackgroundColor(Container.applicationBackground)

        precision = "#."
        for (i in 0 until Container.precision) {
            precision += "#"
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("currentValue", currentValue!!.text.toString())
        outState.putSerializable("values", values)
        outState.putSerializable("undoStack", undoStack)
        outState.putSerializable("cancelStack", cancelStack)
        super.onSaveInstanceState(outState)


    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentValue!!.text = savedInstanceState.getString("currentValue")
        values = savedInstanceState.getSerializable("values") as Stack<Double>
        undoStack = savedInstanceState.getSerializable("undoStack") as Stack<ArrayList<String>>
        cancelStack = savedInstanceState.getSerializable("cancelStack") as Stack<Stack<Double>>
        printValues()
    }


}
