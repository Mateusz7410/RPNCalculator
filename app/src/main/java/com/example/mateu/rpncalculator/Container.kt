package com.example.mateu.rpncalculator

import android.graphics.Color

import java.util.ArrayList
import java.util.Stack

/**
 * Created by mateu on 30.03.2018.
 */

object Container {
    var operationButtonsBackground = Color.WHITE
    var operationButtonsBackgroundS = "White"
    var operationButtonsText = Color.BLACK
    var operationButtonsTextS = "Black"
    var restButtonsBackground = Color.BLACK
    var restButtonsBackgroundS = "Black"
    var restButtonsText = Color.WHITE
    var restButtonsTextS = "White"
    var windowBackground = Color.WHITE
    var windowBackgroundS = "White"
    var windowText = Color.BLACK
    var windowTextS = "Black"
    var applicationBackground = Color.GRAY
    var applicationBackgroundS = "Gray"
    var precision = 4

    var orient = false

}
